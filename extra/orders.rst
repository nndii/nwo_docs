.. _extra/orders/begin:

==================
Действия с заказом
==================

.. note::

   Все действия с заказом, кроме его создания, делаются по запросу :http:patch:`/v2/resources/orders/(objectid:order)`.
   В один запрос одновременно может быть добавленно нескольно действий.
   Все запросы на изменение конкретного заказа, должны делаться синхронно.
   В случае получения запроса до конца обработки предыдущего,
   будет возвращена ошибка :http:statuscode:`409`.

   Это ограничение касается только работы с одним заказом,
   а работать одновременно с несколькими заказами можно.

   В ответ на все PATCH запросы приходит обновленный :ref:`объект заказа <extra/orders/object>` 
   в поле ``data`` и :ref:`ссылки <extra/orders/refs>` в поле ``refs``


.. _extra/orders/customer:

Добавить информацию о покупателе
================================

Информация о покупателе содержится в поле ``customer``. В ней есть следующие поля:

    - **name** (str)
    - **email** (str)
    - **phone** (str)
    - **lang** (en|ru)

.. note::

   В зависимости от выбранной платёжной системы, некоторые из этих полей могут быть обязательными для завершения заказа.

Пример запроса:
    .. sourcecode:: http

       http PATCH https://stage.freetc.net/v2/resources/orders/5b8f85541ad53a000b27b970 Authorization:'key aa44673d78574172ad9a957ff25b27e6' settings:='{"customer": {"name": "Ivan Ivanov", "email": "hello@world.ru", "phone": "+79991234576", "lang": "ru"}}'


.. _extra/orders/vendor_data:

Добавить произвольную информацию в :ref:`объект заказа <extra/orders/object>`
===============================================================================

В поле ``vendor_data`` находятся произвольные поля. Эта информация не используется платформой ticketscloud.
Поле включает в себя:

    - **order_id**  (str) (Необязательно) Номер заказа в системе распространителя. Максимальная длина 64 символа
    - **raw** (object) Объект с произвольными полями.

.. warning::

   | Максимальное кол-во ключей в поле ``raw`` - 20
   | Максимальная длина ключа - 40 символов
   | Все значения - String с максимальной длиной 128 символов.


Пример запроса:
    .. sourcecode:: http

       http PATCH https://stage.freetc.net/v2/resources/orders/5b8f8afd1ad53a000ca85761 Authorization:'key aa44673d78574172ad9a957ff25b27e6' vendor_data:='{"order_id": "asdasdasd123131231", "raw": {"customField": "123123123123", "customField2": "asdasdasd"}}'


.. _extra/orders/send_tickets:

Изменить параметр ``send_tickets``
==================================

Этот параметр отвечает за отправку билетов на почту клиенту.
При значении ``True`` билеты отправляет платформа ticketscloud, на ``email``,
указанный в поле :ref:`customer <extra/orders/customer>`
По умолчанию False, в этом случаи билеты должны отправить вы сами.

Пример запроса:
    .. sourcecode:: http

       http PATCH https://stage.freetc.net/v2/resources/orders/5b8f8afd1ad53a000ca85761 Authorization:'key aa44673d78574172ad9a957ff25b27e6' settings:='{"send_tickets": True}'


.. _extra/orders/promocodes:

Добавить промокоды к заказу
===========================

Изменить список применяемых к заказу промокодов

Пример запроса:
    .. sourcecode:: http

       http PATCH https://stage.freetc.net/v2/resources/orders/5b8f8afd1ad53a000ca85761 Authorization:'key aa44673d78574172ad9a957ff25b27e6' promocodes:='["TOP50"]'



.. _extra/orders/object:

Объект заказа
=============

Объект заказа присылается в поле ``data``

.. sourcecode:: http

   {
        id: objectid
        status: 'executed' | 'in_progress' | 'done' | 'cancelled' | 'expired'
        created_at: isodatetime
        done_at: isodatetime
        expired_after: isodatetime

        origin: 'api' | 'salespoint' | 'widget' | 'control_panel'
        org: objectid
        vendor: objectid
        number: int

        event: objectid

        tickets: [{
            id: objectid
            serial: str
            number: int
            seat: {
                row: str
                number: str
                sector: objectid
            }
            status: 'reserved'
            price: money
            nominal: money
            discount: money
            extra: money
            full: money
        }]

        promocodes: [{
            id: objectid
        }]

        settings: {
            invitation: bool
            send_tickets: bool
            courier: bool
            customer: {
                name: str
                email: email
                phone: str
                lang: 'en' | 'ru'
            }
        }
        sessions: {
            ga: str
            utm: objectid
            roistat: str
        },

        payments: [{
            id: objectid
            type: '_testpay_core' | '_testpay_partner' | 'invoice' | 'platron' | 'cloudpayments' | 'payu' | 'stripe'
            status: 'executed' | 'in_progress' | 'done' | 'cancelled'
            amount: money
        }]

        vendor_data: {
            order_id: str
            raw: {
                ...
            }
        }

        values: {
            price: money
            discount: money
            nominal: money
            extra: money
            full: money
            sets_values: {
                id: {
                    id: objectid
                    price: money  // price
                    nominal: money  // price with discount
                    discount: money  // discount
                    promocode: objectid | None  // link to Promocode
                }
            }
        }
    }

Пример ответа:

.. sourcecode:: http

   "data": {
        "created_at": "2018-09-05 17:04:53",
        "event": "5b7fbba8110fbe0001d5248a",
        "expired_after": "2018-09-05 17:19:53",
        "id": "5b900cb57c23c2000b90a477",
        "number": 40887,
        "org": "5b0286ce517565000d9cb1ca",
        "origin": "api",
        "payments": [],
        "promocodes": [],
        "settings": {
            "invitation": false,
            "send_tickets": false
        },
        "status": "executed",
        "tickets": [],
        "values": {
            "discount": "0.00",
            "extra": "0.00",
            "full": "0.00",
            "nominal": "0.00",
            "price": "0.00",
            "sets_values": {
                "5b7fbba8110fbe0001d5248c": {
                    "discount": "0.00",
                    "id": "5b7fbba8110fbe0001d5248c",
                    "nominal": "100.00",
                    "price": "100.00",
                    "promocode": null
                },
                "5b7fbba8110fbe0001d52491": {
                    "discount": "0.00",
                    "id": "5b7fbba8110fbe0001d52491",
                    "nominal": "300.00",
                    "price": "300.00",
                    "promocode": null
                },
                "5b7fbba8110fbe0001d52496": {
                    "discount": "0.00",
                    "id": "5b7fbba8110fbe0001d52496",
                    "nominal": "200.00",
                    "price": "200.00",
                    "promocode": null
                }
            },
            "viral_promocodes": []
        },
        "vendor": "5b0286ce517565000d9cb1ca",
        "vendor_data": {}
    }


.. _extra/orders/refs:

Ссылки
======

Ссылки присылаются в поле ``refs``.
Это объект с ключами objectid и значениями -- данными.

Пример ответа:

.. sourcecode:: http

   "refs": {
        "events": {
            "5b7fbba8110fbe0001d5248a": {
                "id": "5b7fbba8110fbe0001d5248a",
                "lifetime": {
                    "finish": "2018-09-07 09:00:00",
                    "start": "2018-09-07 07:00:00"
                },
                "org": "5b0286ce517565000d9cb1ca",
                "status": "public",
                "timezone": "Europe/Moscow",
                "title": {
                    "desc": "Короткое описание",
                    "text": "Активный отдых для детей"
                }
            }
        },
        "partners": {
            "5b0286ce517565000d9cb1ca": {
                "id": "5b0286ce517565000d9cb1ca",
                "name": "New organiser"
            },
        },
        "promocodes": {
            "5bf7d86782d1e3000b4ad2aa": {
                "code": "test", 
                "discount": {
                    "percentage": "50%"
                }, 
                "id": "5bf7d86782d1e3000b4ad2aa", 
                "lifetime": null, 
                "sets": [], 
                "tickets_count": {}, 
                "viral": false
            },
        }, 
        "sets": {
            "5bc6f229ee880d000b3fd9eb": {
                "id": "5bc6f229ee880d000b3fd9eb", 
                "name": "Танцевальный партер", 
                "price": "1780.00", 
                "with_seats": false
            },
        },
    }
