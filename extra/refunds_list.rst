.. _extra/refunds_list/begin:

===========================
Работа со списком возвратов
===========================


.. _extra/refunds_list/get:


Получаем список возвратов
=========================

**Описание параметров**

.. http:get:: /v2/resources/refund_requests

   :query created_at: Дата создания возврата, **ISODatetime** промежуток через запятую
   :query finished_at: Дата завершения возврата, **ISODatetime** промежуток через запятую
   :query events: Фильтр по списку мероприятий
   :query status: Фильтр по списку статусов возратов (new | in_progress | approved | rejected)
   :query page:
   :query page_size:

**Пример запроса**

.. sourcecode:: http

   http GET https://stage.freetc.net/v2/resources/refund_requests?created_at=2017,2019\&events=5b0d5591d2a57d000b9eb12f Authorization:'key b4583aafd21346e1a276b0ea4842fd8c'

**Пример ответа**

.. sourcecode:: http

   {
     "data": [
       {
         "created_at": "2018-06-15 15:59:46",
         "culprit": "user",
         "delta": "0.00",
         "event": "5b0d5591d2a57d000b9eb12f",
         "finished_at": "2018-06-15 16:00:07",
         "id": "5b23e272d2a57d00779e5585",
         "order": "5b23e1fdd2a57d000c2bce44",
         "org": "5aba91ccd2a57d000b7832e9",
         "status": "approved",
         "tickets": [
           "5b0d5616d2a57d000179d166",
           "5b0d5616d2a57d000179d165"
         ],
         "vendor": "5aba91ccd2a57d000b7832e9"
       }
     ],
     "pagination": {
       "page": 1,
       "page_size": 50,
       "total": 1
     },
     "refs": {
       "events": {
         "5b0d5591d2a57d000b9eb12f": {
           "id": "5b0d5591d2a57d000b9eb12f",
           "lifetime": {
             "finish": "2018-09-13 13:00:00",
             "start": "2018-09-13 12:00:00"
           },
           "org": "5aba91ccd2a57d000b7832e9",
           "status": "finished",
           "timezone": "Europe/Moscow",
           "title": {
             "desc": "тест",
             "text": "Вебинар"
           }
         }
       },
       "orders": {
         "5b23e1fdd2a57d000c2bce44": {
           "created_at": "2018-06-15 15:57:49",
           "done_at": "2018-06-15 15:57:50",
           "event": "5b0d5591d2a57d000b9eb12f",
           "expired_after": "2018-06-15 16:07:49",
           "id": "5b23e1fdd2a57d000c2bce44",
           "number": 8,
           "org": "5aba91ccd2a57d000b7832e9",
           "origin": "control_panel",
           "payments": [],
           "promocodes": [],
           "settings": {
             "customer": {
               "email": "nik@ticketscloud.org",
               "lang": "ru",
               "name": "Nikolaeva"
             },
             "invitation": true,
             "send_tickets": true
           },
           "status": "done",
           "tickets": [],
           "values": {
             "discount": "0.00",
             "extra": "0.00",
             "full": "0.00",
             "nominal": "0.00",
             "price": "0.00",
             "sets_values": {
               "5b0d55bdd2a57d000c286543": {
                 "discount": "0.00",
                 "id": "5b0d55bdd2a57d000c286543",
                 "nominal": "1500.00",
                 "price": "1500.00",
                 "promocode": null
               },
               "5b0d55e3d2a57d000b9eb133": {
                 "discount": "0.00",
                 "id": "5b0d55e3d2a57d000b9eb133",
                 "nominal": "3400.00",
                 "price": "3400.00",
                 "promocode": null
               },
               "5b0d5616d2a57d000b9eb138": {
                 "discount": "0.00",
                 "id": "5b0d5616d2a57d000b9eb138",
                 "nominal": "5400.00",
                 "price": "5400.00",
                 "promocode": null
               }
             },
             "viral_promocodes": []
           },
           "vendor": "5aba91ccd2a57d000b7832e9",
           "vendor_data": {}
         }
       },
       "partners": {
         "5aba91ccd2a57d000b7832e9": {
           "id": "5aba91ccd2a57d000b7832e9",
           "name": "Tickets Cloud"
         }
       },
       "tickets": {
         "5b0d5616d2a57d000179d165": {
           "discount": "5400.00", 
           "extra": "0.00", 
           "full": "0.00", 
           "id": "5b0d5616d2a57d000179d165", 
           "nominal": "0.00", 
           "number": 110001, 
           "price": "5400.00", 
           "serial": "AKX", 
           "set": "5b0d5616d2a57d000b9eb138", 
           "status": "refunded"
         }, 
         "5b0d5616d2a57d000179d166": {
           "discount": "5400.00", 
           "extra": "0.00", 
           "full": "0.00", 
           "id": "5b0d5616d2a57d000179d166", 
           "nominal": "0.00", 
           "number": 110002, 
           "price": "5400.00", 
           "serial": "AKX", 
           "set": "5b0d5616d2a57d000b9eb138", 
           "status": "refunded"
         }
       }
     }
   }
