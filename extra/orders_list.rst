.. _extra/orders_list/begin:

=========================
Работа со списком заказов
=========================


.. _extra/orders_list/get:

Получаем список заказов
==========================

.. _extra/orders_list/get_params:

**Описание параметров:**

.. http:get:: /v2/resources/orders

   :query created_at: Дата создания ордера в формате ISO от и до, через запятую
   :query status: Список статусов заказа
   :query events: Список мероприятий
   :query only_with_customer: bool; Если True -- Показываем ордеры только если есть кастомер
   :query page:
   :query page_size:


**Пример запроса:**

.. sourcecode:: http

   http GET https://stage.freetc.net/v2/resources/orders?status=done,cancelled&created_at=2000-07-28T13,2020-07-28T13 Authorization:"key 602213fa4ab84f93918f51e87ad6822c"


**Пример ответа:**

.. sourcecode:: http

   {
   "data": [
       {
           "created_at": "2018-08-10 09:46:43",
           "done_at": "2018-08-10 10:01:26",
           "event": "5b6c4e90d9510a00114a4e1e",
           "expired_after": "2018-08-10 10:01:43",
           "id": "5b6d5f0377d428001fd05adc",
           "number": 7,
           "org": "5b6c49acd9510a08b095a754",
           "origin": "api",
           "payments": [],
           "promocodes": [],
           "settings": {
               "invitation": false,
               "send_tickets": false,
           },
           "status": "done",
           "tickets": [
               {
                   "discount": "0.00",
                   "extra": "0.00",
                   "full": "400000.00",
                   "id": "5b6c4e9fd9510a00114a4e26",
                   "nominal": "400000.00",
                   "number": 110000,
                   "price": "400000.00",
                   "serial": "EHY",
                   "set": "5b6c4e9fd9510a00114a4e21",
                   "status": "reserved"
               },
           ],
           "values": {
               "discount": "0.00",
               "extra": "0.00",
               "full": "3200000.00",
               "nominal": "3200000.00",
               "price": "3200000.00",
               "sets_values": {
                   "5b6c4e9fd9510a00114a4e21": {
                       "discount": "0.00",
                       "id": "5b6c4e9fd9510a00114a4e21",
                       "nominal": "400000.00",
                       "price": "400000.00",
                       "promocode": null
                   },
               },
               "viral_promocodes": []
           },
           "vendor": "5b6c49acd9510a08b095a754",
           "vendor_data": {}
       }
   ],
   "pagination": {
       "page": 1,
       "page_size": 50,
       "total": 1
   },
   "refs": {
       "events": {
           "5b6c4e90d9510a00114a4e1e": {
               "id": "5b6c4e90d9510a00114a4e1e",
               "lifetime": {
                   "finish": "2018-08-30 21:59:00",
                   "start": "2018-08-08 22:00:00"
               },
               "org": "5b6c49acd9510a08b095a754",
               "status": "public",
               "timezone": "Europe/Moscow",
               "title": {
                   "desc": "aaa",
                   "text": "BORIS CONFERENCE"
               }
           }
       },
       "partners": {
           "5b6c49acd9510a08b095a754": {
               "id": "5b6c49acd9510a08b095a754",
               "name": "БРЭНД БОРИСА"
           }
       },
       "promocodes": {},
       "legals": {
           "5b6c4abcd9510a0942d1d6a8": {
               "created_at": "2018-08-09 14:07:56",
               "detail": {
                   "address": "3900 Tiffany Lodge Apt. 817\nWest Vickitown, MP 77592",
                   "name": "Lynch and Sons"
               },
               "id": "5b6c4abcd9510a0942d1d6a8",
               "removed": false,
               "type": "ru/ip",
               "updated_at": "2018-08-09 14:07:56",
               "user": "5b6c4abc3f34aa812efa3ef2",
               "who": {
                   "name": "Karen Scott",
                   "position": "matter",
                   "reason": "according"
               }
           }
       }
   }
   }


.. _extra/orders_list/send_to_email:

Отправляем список заказов на почту
=====================================


**Описание параметров:**

Все фильтры из пункта
:ref:`Получение заказов <extra/orders_list/get_params>`
+ обязательное поле ``email``


**Пример запроса:**

.. sourcecode:: http

   http POST https://stage.freetc.net/v2/resources/orders/export \
   status:='["done", "cancelled"]' \
   created_at:='["2000-07-28T13", "2020-07-28T13"]' \
   email="hello@world.hello" \
   Authorization:"key 602213fa4ab84f93918f51e87ad6822c"
