.. _extra/tickets/begin:

=============================
Список билетов по мероприятию
=============================


Все билеты с местами по мероприятию
-----------------------------------

**Описание параметров**

.. http:get:: /v1/resources/events/:event_id/tickets

   :query status: Фильтр-список по статусам (vacant, reserved, sold, pending)
   :query sector: Фильтр-список по секторам


**Пример запроса**

.. sourcecode:: http

   http GET https://stage.freetc.net/v1/resources/events/5b6c4e90d9510a00114a4e1e/tickets?status=sold,vacant&sector=5abccc3dd2a57d00478da4f5 Authorization:"key 602213fa4ab84f93918f51e87ad6822c"


**Пример ответа**

.. sourcecode:: http

   [
       {
        "id": "5b251d12d2a57d000baed3ac",
        "status": "sold",
        "seat": {
            "row": 1,
            "number": 73,
            "sector": "5abccc3dd2a57d00478da4f5"
        },
        "set": "5b251d50d2a57d000baee90b"
       },
       {
        "id": "5b251d12d2a57d000baed3b1",
        "status": "sold",
        "seat": {
            "row": 1,
            "number": 78,
            "sector": "5abccc3dd2a57d00478da4f5"
        },
        "set": "5b251d50d2a57d000baee90b"
       }
   ]
