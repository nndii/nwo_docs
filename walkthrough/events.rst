.. _walkthrough/events/begin:

========================================
Шаг 1: Информация о мероприятии
========================================


.. _walkthrough/events/simple:

Получаем список мероприятий
===========================


Мероприятия можно получить запросом :http:get:`/v1/services/simple/events`
В поле `events` содержится список евентов, по которым у вас заключена сделка с организатором.
Остальные поля представляют собой объекты с информацией о каждой сущности,
присутствующей на данной странице в любом из евентов.

Пример запроса:

.. sourcecode:: http

   http GET https://stage.freetc.net/v1/services/simple/events Authorization:"key 602213fa4ab84f93918f51e87ad6822c"


Описание полей ответа:

    - ``id`` **str**:*ObjectId* - id мероприятия
    - ``created_at`` **str**:*ISODatetime* - дата создания
    - ``updated_at`` **str**:*ISODatetime* - дата последнего изменения
    - ``lifetime`` **str**:*VEVENT*  - :ref:`vevent <extra/types/vevent>`, время проведения мероприятия
    - ``status`` **str** - текущий статус мероприятия. `public` — публичное мероприятие, можно продавать билеты)

    - ``title`` **object**
    
       - ``text`` **str** - название мероприятия
       - ``desc`` **str** - описание мероприятия

    - ``media`` :ref:`media <extra/types/media>` логитипы в различных размерах:

       - ``cover_original`` **object**:*media*
       - ``cover`` **object**:*media*
       - ``cover_small`` **object**:*media*

    - ``org`` **object** информация об организаторе мероприятия

       - ``id`` **str**:*ObjectId* - id организатора
       - ``name`` **str** - название организатора
       - ``desc`` **str** - краткое описание
       - ``media`` **object** - media логотипы
       - ``contact`` - контактная информация
         
    - ``venue`` место проведения
      
       - ``id`` **str**:*ObjectId* - venue id
       - ``address`` **str** - адрес
       - ``country`` :ref:`страна <extra/types/country>`
       - ``city`` :ref:`город <extra/types/city>`
       - ``name`` **str** - название
       - ``desc`` **str** - краткое описание
       - ``point`` координата (`GeoJSON <http://geojson.org>`_'s point)

    - ``map`` схема зала

    - ``sets`` билетные категории

       - ``id``
       - ``pos`` **int** - порядковый номер категории (для сортировки)
       - ``name`` **str** - название категории
       - ``amount`` **int** - общее количество билетов в сете
       - ``amount_vacant`` **int** - количество билетов, доступных для продажи
       - ``price_org`` **str**:*Money* - номинальная цена билета
       - ``price_extra`` **str**:*Money* - сервисный сбор
       - ``price`` **str**:*Money* - общая цена билета
       - ``with_seats`` **bool** - наличие посадочных мест в категории
       - ``seats`` **object** - row: numbers (**list**)
       - ``sector`` сектор

    - ``rules`` правила

       - ``id``
       - ``cal`` :ref:`vevent <extra/types/vevent>`, время действия правила
       - ``current`` **bool** - `true` если правило текущее
       - ``price_org`` **str**:*Money* - номинальная цена
       - ``price_extra`` - сервисный сбор
       - ``price`` **str**:*Money* - конечная цена


Пример ответа:

.. sourcecode:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=UTF-8
    X-Partner: 56810047f06c5a6ac62f4e1d

    [
        {
            "id": "568a22b5f06c5a42975fb913",
            "age_rating": 0, 
            "created_at": "2016-01-04T07:43:49.793000+00:00", 
            "deal": null,  
            "lifetime": "BEGIN:VEVENT\r\nDTSTART;VALUE=DATE-TIME:20160127T210000Z\r\nDTEND;VALUE=DATE-TIME:20160128T205900Z\r\nEND:VEVENT\r\n", 
            "map": null, 
            "media": {}, 
            "org": {
                "id": "56810047f06c5a6ac62f4e1d",
                "contact": {
                    "address": "Россия г. Москва, ул. Жуковского д. 17 к. 2, 6UYDD", 
                    "email": "vsuharnikov+6uydd@gmail.com", 
                    "name": "", 
                    "phones": [
                        "8-916-768-32-32"
                    ], 
                    "www": "v_test_6UYDD.com"
                }, 
                "desc": "description", 
                "media": {}, 
                "name": "v_test_6UYDD brand", 
                "tags": []
            }, 
            "partner": {
                "id": "56810047f06c5a6ac62f4e1d",
                "contact": {
                    "address": "Россия г. Москва, ул. Жуковского д. 17 к. 2, 6UYDD", 
                    "email": "vsuharnikov+6uydd@gmail.com", 
                    "name": "", 
                    "phones": [
                        "8-916-768-32-32"
                    ], 
                    "www": "v_test_6UYDD.com"
                }, 
                "desc": "description", 
                "media": {}, 
                "name": "v_test_6UYDD brand", 
                "tags": []
            }, 
            "sets": [
                {
                    "id": "568a22b6f06c5a42985fb914",
                    "amount": 3, 
                    "amount_vacant": 1,  
                    "name": "fdfd", 
                    "pos": 0, 
                    "price": "100.00", 
                    "price_extra": "0.00", 
                    "price_org": "100.00", 
                    "rules": [
                        {
                            "id": "56a61828f06c5a059b937fdc",
                            "cal": "BEGIN:VEVENT\r\nDTSTART;VALUE=DATE-TIME:20160102T210000Z\r\nDTEND;VALUE=DATE-TIME:20160128T205900Z\r\nEND:VEVENT\r\n", 
                            "current": true,  
                            "price": "100.00", 
                            "price_extra": "0.00", 
                            "price_org": "100.00"
                        }
                    ], 
                    "seats": null, 
                    "sector": null
                }
            ], 
            "status": "public", 
            "tags": [
                "выставки"
            ], 
            "ticket_template": {
                "fan_cover_url": null, 
                "name": null, 
                "text_color": null
            }, 
            "tickets_amount": 3, 
            "tickets_amount_vacant": 1, 
            "title": {
                "desc": "test", 
                "text": "test"
            }, 
            "updated_at": "2016-01-25T13:33:04.583000+00:00", 
            "venue": {
                "id": "554111c09cb538793e6a3c37",
                "address": "Пресненский вал, дом 6, строение 1", 
                "city": {
                    "id": 524901,
                    "country": "RU",  
                    "name": {
                        "be": "Горад Масква", 
                        "default": "Moscow", 
                        "en": "Moscow", 
                        "fr": "Moscou", 
                        "ru": "Москва", 
                        "zh": "莫斯科"
                    }, 
                    "timezone": "Europe/Moscow"
                }, 
                "country": {
                    "id": "RU", 
                    "name": {
                        "be": "Расійская Федэрацыя", 
                        "default": "Russia", 
                        "en": "Russia", 
                        "fr": "Russie", 
                        "ru": "Россия", 
                        "zh": "俄罗斯"
                    }
                }, 
                "desc": null, 
                "name": "16 тонн", 
                "point": {
                    "coordinates": [
                        37.56434200000001, 
                        55.76430800000001
                    ], 
                    "type": "Point"
                }
            }
        }, 
        {
            "id": "56a6253df06c5a059a93802e",
            "age_rating": 0, 
            "created_at": "2016-01-25T13:38:05.007000+00:00", 
            "deal": null,
            "lifetime": "BEGIN:VEVENT\r\nDTSTART;VALUE=DATE-TIME:20160512T200000Z\r\nDTEND;VALUE=DATE-TIME:20160513T195900Z\r\nEND:VEVENT\r\n", 
            "map": {
                "id": "54d79ee69cb538749c32c221",
                "desc": null,  
                "name": "default", 
                "sectors": [
                    {
                        "id": "54d7a0409cb538783b7bf8d5",
                        "desc": null,  
                        "name": "Партер"
                    }, 
                    {
                        "id": "54d7a0409cb538783b7bf8d6",
                        "desc": null, 
                        "name": "Балкон"
                    }
                ], 
                "svg": {
                    "map": {
                        "id": "54d7a0409cb538783b7bf8d7",
                        "author": null, 
                        "content_type": "image/svg+xml",  
                        "length": null, 
                        "md5hash": "9efaa9cf8af50f95a3ddf205f7bcebe0", 
                        "url": "https://s3-eu-west-1.amazonaws.com:443/media.ticketscloud/production/map/2015-02/54d79ee69cb538749c32c221-54d79ee69cb538749c32c220.svg"
                    }, 
                    "mapz": {
                        "id": "561c43a79cb5380fdcab402d",
                        "author": null, 
                        "content_type": "image/svg+xml", 
                        "length": null, 
                        "md5hash": "c77114baa0912d621044e8ad17f8aede", 
                        "url": "https://s3-eu-west-1.amazonaws.com:443/media.ticketscloud/production/map/2015-10/54d79ee69cb538749c32c221-54d79ee69cb538749c32c220.svgz"
                    }, 
                    "source": {
                        "id": "54d79ee69cb538749c32c220",
                        "author": null, 
                        "content_type": "image/svg+xml",  
                        "length": null, 
                        "md5hash": "d5bc921d747322b599f78987fa492c0b", 
                        "url": "https://s3-eu-west-1.amazonaws.com:443/media.ticketscloud/production/maps/2015-02/54d79ee69cb538749c32c220.svg"
                    }
                }
            }, 
            "media": {}, 
            "org": {
                "id": "56810047f06c5a6ac62f4e1d",
                "contact": {
                    "address": "Россия г. Москва, ул. Жуковского д. 17 к. 2, 6UYDD", 
                    "email": "vsuharnikov+6uydd@gmail.com", 
                    "name": "", 
                    "phones": [
                        "8-916-768-32-32"
                    ], 
                    "www": "v_test_6UYDD.com"
                }, 
                "desc": "description", 
                "media": {}, 
                "name": "v_test_6UYDD brand", 
                "tags": []
            }, 
            "partner": {
                "id": "56810047f06c5a6ac62f4e1d",
                "contact": {
                    "address": "Россия г. Москва, ул. Жуковского д. 17 к. 2, 6UYDD", 
                    "email": "vsuharnikov+6uydd@gmail.com", 
                    "name": "", 
                    "phones": [
                        "8-916-768-32-32"
                    ], 
                    "www": "v_test_6UYDD.com"
                }, 
                "desc": "description", 
                "media": {}, 
                "name": "v_test_6UYDD brand", 
                "tags": []
            }, 
            "sets": [
                {
                    "id": "56a6254bf06c5a059b93800c",
                    "amount": 3, 
                    "amount_vacant": 3,  
                    "name": "Партер", 
                    "pos": 0, 
                    "price": "100.00", 
                    "price_extra": "0.00", 
                    "price_org": "100.00", 
                    "rules": [
                        {
                            "id": "56a6254bf06c5a059b93800b",
                            "cal": "BEGIN:VEVENT\r\nDTSTART;VALUE=DATE-TIME:20160123T200000Z\r\nDTEND;VALUE=DATE-TIME:20160513T195900Z\r\nEND:VEVENT\r\n", 
                            "current": true, 
                            "price": "100.00", 
                            "price_extra": "0.00", 
                            "price_org": "100.00"
                        }
                    ], 
                    "seats": {
                        "1": [
                            [
                                1, 
                                1
                            ], 
                            [
                                3, 
                                3
                            ], 
                            [
                                6, 
                                6
                            ]
                        ]
                    }, 
                    "sector": "54d7a0409cb538783b7bf8d5"
                }
            ], 
            "status": "public", 
            "tags": [
                "экскурсии"
            ], 
            "ticket_template": {
                "fan_cover_url": null, 
                "name": null, 
                "text_color": null
            }, 
            "tickets_amount": 3, 
            "tickets_amount_vacant": 3, 
            "title": {
                "desc": null, 
                "text": "With seats"
            }, 
            "updated_at": "2016-01-25T13:38:19.844000+00:00", 
            "venue": {
                "id": "54d49b9df06c5a0dbde10e7f",
                "address": "443010, г. Самара, ул. Фрунзе, д. 141", 
                "city": {
                    "id": 499099,
                    "country": "RU",
                    "name": {
                        "be": "Горад Самара", 
                        "default": "Samara", 
                        "en": "Samara", 
                        "fr": "Samara", 
                        "ru": "Самара", 
                        "zh": "薩馬拉"
                    }, 
                    "timezone": "Europe/Samara"
                }, 
                "country": {
                    "id": "RU", 
                    "name": {
                        "be": "Расійская Федэрацыя", 
                        "default": "Russia", 
                        "en": "Russia", 
                        "fr": "Russie", 
                        "ru": "Россия", 
                        "zh": "俄罗斯"
                    }
                }, 
                "desc": null, 
                "name": "Самарская Государственная Филармония", 
                "point": {
                    "coordinates": [
                        50.09498499999995, 
                        53.19151799999999
                    ], 
                    "type": "Point"
                }
            }
        }
    ]


.. _walkthrough/events/tickets:

Получаем список билетов с местами по мероприятию
================================================

Получение списка билетов мероприятия, для категорий с рассадкой.

.. http:post:: /v1/resources/events/:id/tickets

   :query status: Фильтр по списку статусов (`vacant` | `reserved` | `sold` | `pending`). По умолчанию включены билеты во всех статусах, кроме `pending`.
   :query sector: Фильтр по списку секторов


**Описание полей ответа:**

:id: id билета
:status: одно из ``vacant``, ``reserved`` или ``sold``
:set: билетная категория
:reserved_till: если статус ``reserved``, то это время окончания конца резервирования
:seat: Информация о месте проведения

    :row: ряд
    :number: место
    :sector: сектор карты


**Пример запроса:**

.. sourcecode:: http

   http GET https://stage.freetc.net/v1/resources/events/56a6253df06c5a059a93802e/tickets?status=vacant,sold&sector=54d7a0409cb538783b7bf8d5


**Пример ответа:**

.. sourcecode:: http

   HTTP/1.1 200 OK
   Content-Type: application/json; charset=UTF-8
   X-Partner: 56810047f06c5a6ac62f4e1d
   
   [
     {
       "id": "56a6253df06c5a059a9380a0", 
       "reserved_till": null, 
       "seat": {
         "number": 1, 
         "row": 1, 
         "sector": "54d7a0409cb538783b7bf8d5"
       }, 
       "set": "56a6254bf06c5a059b93800c", 
       "status": "vacant"
     }, 
     {
       "id": "56a6253df06c5a059a9380a4", 
       "reserved_till": null, 
       "seat": {
         "number": 3, 
         "row": 1, 
         "sector": "54d7a0409cb538783b7bf8d5"
       }, 
       "set": "56a6254bf06c5a059b93800c", 
       "status": "vacant"
     }, 
   ]


.. _walkthrough/events/widget:

Получаем информацию для отображения виджета Мероприятия
=======================================================


Информацию для отображения виджета можно получить запросом :http:post:`/v1/services/widget`
с параметрами `event` или `meta_event` в случае метаевента.

Пример запроса:

.. sourcecode:: http

   http POST https://stage.freetc.net/v1/services/widget Authorization:'key aa44673d78574172ad9a957ff25b27e6' event=5b34f8745c60ee000c67f409


Описание полей ответа:

    - ``vendor`` **str**:*ObjectId* - id распространителя
    - ``org`` **str**:*ObjectId* - id организатора
    - ``meta_event`` **str**:*ObjectId* | **null** - cсылка на метаевент 
    - ``event`` **object** объект евента :ref:`объект евента <walkthrough/events/simple>`
    - ``settings`` **object** объект с настройками евента
    - ``sets`` **object** объект с категориями билетов где ключ - id категории, значение объект категории:

        - ``id`` **str**:*ObjectId* - id категории
        - ``name`` **str**
        - ``desc`` **str**
        - ``pos``
        - ``sector`` **str**:*ObjectId* - id сектора
        - ``amount`` **int** - кол-во билетов
        - ``amount_vacant`` **int** - кол-во билетов в статусе ``vacant``
        - ``with_seats`` **bool** - Категория с рассадкой/без
        - ``prices`` **array** - Список правил динамического ценообразования

    - ``tickets`` **object** - Объект с билетами где ключ - id сектора, 
        значение - объект с ключами -- рядами значениями билетами:

        - ``id`` **str**:*ObjectId* - id билета
        - ``set`` **str**:*ObjectId* - id категории
        - ``status`` **str**:*ObjectdId* - Статус билета
        - ``reserved_till``
    
    - ``venue`` **object** - Объект с информацией о месте проведения мероприятия:

        - ``id`` **str**:*ObjectId** - id площадки
        - ``name`` **str**
        - ``desc`` **str**
        - ``address`` **str**
        - ``point`` **object**
        - ``country`` **object**
        - ``city`` **object**
    
    - ``map`` **object**
    - ``partners`` **object**
    - ``payment_settings`` **object**
    - ``tz`` **str** таймзона
    - ``ga_id`` **str** Google Analytics id
    - ``ym_id`` **str** Yandex Metrics id
    - ``vk_pixel`` **str** VK pixel id
    - ``fb_pixel`` **str** Facebook Pixel id
    - ``has_promocodes`` **bool**
    - ``kryptonite_send`` **bool**
    - ``lang_switcher`` **bool**
    - ``viral_promocodes_enabled`` **bool**
        

Пример ответа:

.. sourcecode:: http

   {
        "vendor": "5b0286ce517565000d9cb1ca",
        "org": "5b0286ce517565000d9cb1ca",
        "meta_event": null,
        "event": {
            "id": "5b34f8745c60ee000c67f409",
            "title": {
                "text": "FACE \u0432 \u041c\u043e\u0441\u043a\u0432\u0435",
                "desc": "\u041d\u043e\u0432\u044b\u0439 \u0442\u0443\u0440 FACE\n\n\u0412\u043e\u0437\u0440\u0430\u0441\u0442\u043d\u043e\u0435 \u043e\u0433\u0440\u0430\u043d\u0438\u0447\u0435\u043d\u0438\u0435: 16+"
            },
            "age_rating": 16,
            "media": {},
            "org": "5b0286ce517565000d9cb1ca",
            "lifetime": {
                "start": "2018-11-30T17:00:00+00:00",
                "finish": "2018-11-30T19:00:00+00:00"
            },
            "widget_ext": null,
            "tickets_limit": null,
            "category": "592841f8515e35002dead938",
            "tags": [
                "592841f8515e35002dead94a",
                "592841f8515e35002dead93b"
            ]
        },
        "settings": {
            "translator": false,
            "show_cover": false,
            "show_description": false,
            "white_label": false,
            "price_change": false,
            "tickets_left": 10,
            "sets_to_show": 3,
            "support_phone": null,
            "support_email": null,
            "css_link": null,
            "contract_link": null,
            "redirect_link": null
        },
        "sets": {
            "5b34f8765c60ee000c67f553": {
                "id": "5b34f8765c60ee000c67f553",
                "name": "\u0422\u0430\u043d\u0446\u0435\u0432\u0430\u043b\u044c\u043d\u044b\u0439 \u043f\u0430\u0440\u0442\u0435\u0440",
                "desc": "\u0411\u0438\u043b\u0435\u0442 \u0440\u0430\u0441\u0441\u0447\u0438\u0442\u0430\u043d \u043d\u0430 \u043f\u043e\u0441\u0435\u0449\u0435\u043d\u0438\u0435 \u0437\u043e\u043d\u044b \u0442\u0430\u043d\u0446\u043f\u043e\u043b\u0430 \u0432 \u043e\u0434\u043d\u043e\u043c \u043b\u0438\u0446\u0435.\n\u0412\u043e\u0437\u0440\u0430\u0441\u0442\u043d\u043e\u0435 \u043e\u0433\u0440\u0430\u043d\u0438\u0447\u0435\u043d\u0438\u0435: 16+",
                "pos": 0,
                "sector": "5b2930886e55b206059b760b",
                "amount": 26,
                "amount_vacant": 23,
                "with_seats": false,
                "prices": [
                    {
                        "start": "2018-06-18 21:00:00",
                        "finish": "2018-11-30 19:00:00",
                        "nominal": "800.00",
                        "extra": "0.00",
                        "full": "800.00"
                    }
                ]
            },
            "tickets": {
                "5b2930886e55b206059b760f": {
                    "21": {
                        "168": {
                            "id": "5b34f8745c60ee000c67f529",
                            "set": "5b34f8775c60ee000c67f557",
                            "status": "sold",
                            "reserved_till": null
                        },
                        "166": {
                            "id": "5b34f8745c60ee000c67f527",
                            "set": "5b34f8775c60ee000c67f557",
                            "status": "vacant",
                            "reserved_till": null
                        },
                    },
                    "22": {
                        "175": {
                            "id": "5b34f8745c60ee000c67f530",
                            "set": "5b34f8775c60ee000c67f557",
                            "status": "sold",
                            "reserved_till": null
                        },
                        "173": {
                            "id": "5b34f8745c60ee000c67f52e",
                            "set": "5b34f8775c60ee000c67f557",
                            "status": "vacant",
                            "reserved_till": null
                        },
                    }
                }
            },
            "venue": {
                "id": "5ad9abd9d35286001a4f8991",
                "name": "Cition Hall",
                "desc": "",
                "address": "\u0428\u043c\u0438\u0442\u043e\u0432\u0441\u043a\u0438\u0439 \u043f\u0440\u043e\u0435\u0437\u0434, 32\u0410 \u0441\u0442\u0440\u043e\u0435\u043d\u0438\u0435 1",
                "point": {
                    "type": "Point",
                    "coordinates": [
                        37.53011900000001,
                        55.75682
                    ]
                },
                "country": {
                    "iso": "RU",
                    "iso3": "RUS",
                    "name": {
                        "en": "Russia",
                        "ru": "\u0420\u043e\u0441\u0441\u0438\u044f"
                    }
                },
                "city": {
                    "name": {
                        "en": "Moscow",
                        "ru": "\u041c\u043e\u0441\u043a\u0432\u0430"
                    }
                }
            },
            "map": {
                "id": "5b2930886e55b206059b760a",
                "name": "\u0421\u0445\u0435\u043c\u0430 \u0441 \u0440\u0430\u0441\u0441\u0430\u0434\u043a\u043e\u0439 \u043f\u043e \u043c\u0435\u0441\u0442\u0430\u043c",
                "desc": "",
                "sectors": [
                    {
                        "id": "5b2930886e55b206059b760b",
                        "name": "\u0422\u0430\u043d\u0446\u0435\u0432\u0430\u043b\u044c\u043d\u044b\u0439 \u043f\u0430\u0440\u0442\u0435\u0440",
                        "desc": "",
                        "with_seats": false,
                        "seats": null,
                        "type": "chairs"
                    },
                    {
                        "id": "5b2930886e55b206059b760c",
                        "name": "VIP LEFT",
                        "desc": "",
                        "with_seats": true,
                        "seats": {
                            "1": [
                                [
                                    1,
                                    8
                                ]
                            ],
                            "2": [
                                [
                                    9,
                                    16
                                ]
                            ],
                            "3": [
                                [
                                    17,
                                    24
                                ]
                            ],
                            "4": [
                                [
                                    25,
                                    32
                                ]
                            ]
                        },
                        "type": "chairs"
                    },
                    {
                        "id": "5b2930886e55b206059b760d",
                        "name": "VIP RIGHT",
                        "desc": "",
                        "with_seats": true,
                        "seats": {
                            "1": [
                                [
                                    1,
                                    8
                                ]
                            ],
                            "2": [
                                [
                                    9,
                                    16
                                ]
                            ],
                            "3": [
                                [
                                    17,
                                    24
                                ]
                            ],
                            "4": [
                                [
                                    25,
                                    32
                                ]
                            ]
                        },
                        "type": "chairs"
                    },
                    {
                        "id": "5b2930886e55b206059b760e",
                        "name": "SUPER VIP",
                        "desc": "",
                        "with_seats": true,
                        "seats": {
                            "1": [
                                [
                                    1,
                                    8
                                ]
                            ],
                            "2": [
                                [
                                    9,
                                    16
                                ]
                            ],
                            "3": [
                                [
                                    17,
                                    24
                                ]
                            ],
                            "4": [
                                [
                                    25,
                                    32
                                ]
                            ],
                            "5": [
                                [
                                    33,
                                    40
                                ]
                            ],
                            "6": [
                                [
                                    41,
                                    48
                                ]
                            ],
                            "7": [
                                [
                                    49,
                                    56
                                ]
                            ]
                        },
                        "type": "chairs"
                    },
                    {
                        "id": "5b2930886e55b206059b760f",
                        "name": "VIP CENTER",
                        "desc": "",
                        "with_seats": true,
                        "seats": {
                            "1": [
                                [
                                    1,
                                    8
                                ]
                            ],
                            "2": [
                                [
                                    9,
                                    16
                                ]
                            ],
                            "3": [
                                [
                                    17,
                                    24
                                ]
                            ],
                            "4": [
                                [
                                    25,
                                    32
                                ]
                            ],
                            "5": [
                                [
                                    33,
                                    40
                                ]
                            ],
                            "6": [
                                [
                                    41,
                                    48
                                ]
                            ],
                            "7": [
                                [
                                    49,
                                    56
                                ]
                            ],
                            "8": [
                                [
                                    57,
                                    64
                                ]
                            ],
                            "9": [
                                [
                                    65,
                                    72
                                ]
                            ],
                            "10": [
                                [
                                    73,
                                    80
                                ]
                            ],
                            "11": [
                                [
                                    81,
                                    88
                                ]
                            ],
                            "12": [
                                [
                                    89,
                                    96
                                ]
                            ],
                            "13": [
                                [
                                    97,
                                    104
                                ]
                            ],
                            "14": [
                                [
                                    105,
                                    112
                                ]
                            ],
                            "15": [
                                [
                                    113,
                                    120
                                ]
                            ],
                        },
                        "type": "chairs"
                    }
                ],
                "svg": {
                    "source": "https://ticketscloud.org/s3/media.ticketscloud/production/map/2018-06/5b2930886e55b206059b7609.svg",
                    "map_main_svg": "https://ticketscloud.org/s3/media.ticketscloud/production/map/2018-06/5b2930886e55b206059b760a-5b2930886e55b206059b7609-main.svg",
                    "map_main_svgz": "https://ticketscloud.org/s3/media.ticketscloud/production/map/2018-06/5b2930886e55b206059b760a-5b2930886e55b206059b7609-main.svgz",
                    "map": "https://ticketscloud.org/s3/media.ticketscloud/production/map/2018-06/5b2930886e55b206059b760a-5b2930886e55b206059b7609-main.svg",
                    "mapz": "https://ticketscloud.org/s3/media.ticketscloud/production/map/2018-06/5b2930886e55b206059b760a-5b2930886e55b206059b7609-main.svgz"
                }
            },
            "partners": {
                "5b0286ce517565000d9cb1ca": {
                    "id": "5b0286ce517565000d9cb1ca",
                    "role": "org",
                    "name": "New organiser",
                    "desc": "Test",
                    "media": {},
                    "currency": "RUB",
                    "legal": {
                        "name": "\u041e\u0410\u041e \"\u041f\u0435\u0440\u0432\u0430\u044f \u0420\u0430\u0437\u0432\u043b\u0435\u043a\u0430\u0442\u0435\u043b\u044c\u043d\u0430\u044f \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u044f\"",
                        "address": "\u0433. \u041c\u043e\u0441\u043a\u0432\u0430, \u0413\u0440\u0430\u0444\u0441\u043a\u0438\u0439 \u043f\u0435\u0440\u0435\u0443\u043b\u043e\u043a, \u0434\u043e\u043c 14, \u0441\u0442\u0440\u043e\u0435\u043d\u0438\u0435 2, 4 \u044d\u0442\u0430\u0436",
                        "inn": "2345423521",
                        "ogrn": "3452340982112",
                        "ogrnip": null,
                        "type": "ru/ltd"
                    }
                }
            },
            "payment_settings": {
                "invoice": {
                    "type": "invoice",
                    "core": true,
                    "testing": false
                },
                "cloudpayments": {
                    "type": "cloudpayments",
                    "core": true,
                    "testing": true,
                    "merchant": "pk_aab8ffc2acac0d2bb3400671c832f",
                    "applepay_id": null
                }
            },
            "tz": "Europe/Moscow",
            "ga_id": null,
            "ym_id": null,
            "fb_pixel": null,
            "vk_pixel": null,
            "has_promocodes": true,
            "kryptonite_send": false,
            "lang_switcher": true,
            "viral_promocodes_enabled": true
        }
