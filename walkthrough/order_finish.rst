.. _walkthrough/order_finish/begin:

==========================
Шаг 3.1: Завершение заказа
==========================

.. note::

   На данном шаге рассматривается завершение заказа без работы
   с платежными системами. Для подробной информации по интеграции с
   платежными системами можно :ref:`Перейти к следующему шагу <walkthrough/order_payments/begin>`


.. _walkthrough/order_finish/status:

Смена статуса заказа
====================

Заказ завершается запросом :http:patch:`/v2/resources/orders/` с параметром ``status``

Возможные значения для параметра ``status``:
    - ``done``
    - ``cancelled``

Пример запроса:
    .. sourcecode:: http

       http PATCH https://stage.freetc.net/v2/resources/orders Authorization:'key 602213fa4ab84f93918f51e87ad6822c' status=done

Пример ответа:
    .. sourcecode:: http

       HTTP/1.1 200 OK

       {
         {
         "data": {
         "created_at": "2018-06-22 11:40:13",
         "event": "5afc268bd2a57d0001690855",
         "expired_after": "2018-06-22 11:55:13",
         "id": "5b2ce01df62362000dfb2241",
         "number": 81,
         "org": "5aba91ccd2a57d000b7832e9",
         "origin": "api",
         "payments": [],
         "promocodes": [],
         "settings": {
            "courier": false,
            "invitation": false,
            "send_tickets": false
         },
         "status": "executed",
         "tickets": [],
         "values": {},
         "vendor": "5afc08ded2a57d000cddf1ba",
         "vendor_data": {}
         },
       "refs": {}
      }


.. seealso::

   :ref:`Жизненный цикл заказа <extra/lifecycle/begin>`

.. warning::

   Заказ в статусе ``done`` больше нельзя изменять.
   Любые ``PATCH`` запросы на данный заказ будут отклонены со статусом
   :http:statuscode:`400`.

Поздравляем! Вы провели свой первый заказ через API!
