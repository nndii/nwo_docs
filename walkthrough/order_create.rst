.. _walkthrough/order_create/begin:

======================
Шаг 2: Создание заказа
======================

.. note::

   Примеры ответов и запросов в данном разделе упрощены.
   Тем не менее этого достаточно для интеграции.
   Детальная информация по каждому разделу присутствует в блоках *См. также*




.. _walkthrough/order_create/create:

Создание
========

Заказ создается запросом :http:post:`/v2/resources/orders/` с обязательным параметром ``event``

Пример запроса:
    .. sourcecode:: http

       http POST https://stage.freetc.net/v2/resources/orders Authorization:'key 602213fa4ab84f93918f51e87ad6822c' event=507f191e810c19729de860ea

Пример ответа:
    .. sourcecode:: http

       HTTP/1.1 200 OK

       {
         {
         "data": {
         "created_at": "2018-06-22 11:40:13",
         "event": "5afc268bd2a57d0001690855",
         "expired_after": "2018-06-22 11:55:13",
         "id": "5b2ce01df62362000dfb2241",
         "number": 81,
         "org": "5aba91ccd2a57d000b7832e9",
         "origin": "api",
         "payments": [],
         "promocodes": [],
         "settings": {
            "courier": false,
            "invitation": false,
            "send_tickets": false
         },
         "status": "executed",
         "tickets": [],
         "values": {},
         "vendor": "5afc08ded2a57d000cddf1ba",
         "vendor_data": {}
         },
       "refs": {}
      }


.. seealso::

   :ref:`Заказ в статусе executed <extra/lifecycle/executed>`,
   :ref:`Жизненный цикл заказа <extra/lifecycle/begin>`



.. _walkthrough/order_create/tickets:

Резервирование билетов
======================

.. note::

   Все действия с заказом, кроме его создания, делаются по запросу :http:patch:`/v2/resources/orders/:id`.
   В один запрос одновременно может быть добавленно нескольно действий.
   Все запросы на изменение конкретного заказа, должны делаться синхронно.
   В случае получения запроса до конца обработки предыдущего,
   будет возвращена ошибка :http:statuscode:`409`.

   Это ограничение касается только работы с одним заказом,
   а работать одновременно с несколькими заказами можно.

За резервирование билетов отвечают три поля:

   - :ref:`tickets <walkthrough/order_create/ticket>`
   - :ref:`random <walkthrough/order_create/random>`
   - :ref:`all_or_nothing <walkthrough/order_create/all_or_nothing>`


.. _walkthrough/order_create/ticket:

Поле ``tickets``
----------------

В поле tickets передаются все id :ref:`билетов <extra/ticket/object>`, которые должны быть зарезервированы
текущим заказом. Если заказ изменяется (покупатель решил добавить ещё один билет),
то в обязательном порядке передаются все билеты,
которые должны быть в заказе (в т.ч. те, что уже зарезервированы).
Для удаления конкретного билета из заказа, нужно передать все билеты, кроме удаляемого.

.. warning:: 

   Нельзя использовать в одном запросе с :ref:`random <walkthrough/order_create/random>`.

Пример запроса:
   .. sourcecode:: http

      http PATCH https://stage.freetc.net/v2/resources/orders/5b2ce01df62362000dfb2241 Authorization:'key 602213fa4ab84f93918f51e87ad6822c' tickets:="['blabla']"


.. _walkthrough/order_create/random:

Поле ``random``
---------------

Поле random предназначено для резервирования 
случайных билетов из указанных :ref:`категорий <extra/ticketset/object>`.
Оно нужно для того, чтобы добавлять в заказ билеты без мест.
Имеет вид объекта, где ключ является id категории, а значение — количество билетов.
Так же, как и в tickets, всегда нужно передавать желаемое состояние.
Т.е. если пользователь удалил один билет из категории,
то передать надо random со всеми категориями и количествами, только в одной из категорий будет на один билет меньше.
В ответе от сервера всегда будет список забронированных билетов в поле tickets.

.. warning::

   Нельзя использовать в одном запросе с :ref:`tickets <walkthrough/order_create/ticket>`.

Пример запроса:
   .. sourcecode:: http

      http PATCH https://stage.freetc.net/v2/resources/orders/5b2ce01df62362000dfb2241 Authorization:'key 602213fa4ab84f93918f51e87ad6822c' random:="{'category_id': 2}"


.. _walkthrough/order_create/all_or_nothing:

Поле ``all_or_nothing``
-----------------------

Если поле ``all_or_nothing`` ``true``, то резервируются либо все билеты, либо ни одного.

При изменении заказа с одновременным разрезервированием и резервированием билетов,
в случае неудачи с резервированием хотя бы одного билета, разрезервирования не происходит,
т.е. список зарезервированных билетов не изменяется.

Если значение не указано, или ``false``, то билеты,
которые не удалось забронировать пропускаются и
отсутствуют поле :ref:`tickets <walkthrough/order_create/ticket>` в ответе.

.. note::

   Можно использовать, как с ``tickets``, так и с ``random``
